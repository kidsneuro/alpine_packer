# Alpine - Build toolset

Primarily built to facilitate Google Compute image builds<sup>1</sup>, this is a lightweight image that enables use of a number of toolsets in a containerised manner.

- [packer](https://www.packer.io/)
- [Ansible](https://docs.ansible.com/)
- OpenSSH

<sup>1</sup> https://cloud.google.com/build/docs/building/build-vm-images-with-packer#yaml

