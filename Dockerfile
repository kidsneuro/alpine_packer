FROM alpine:latest

ARG PACKER_VERSION

WORKDIR /tmp

RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip && \
    /usr/bin/unzip packer_${PACKER_VERSION}_linux_amd64.zip && \
    mv packer /usr/bin && \
    rm /tmp/packer_${PACKER_VERSION}_linux_amd64.zip

RUN apk add --no-cache ansible openssh

ENTRYPOINT ["/usr/bin/packer"]